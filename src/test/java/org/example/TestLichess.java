import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestYandexMusic {

    @Test
    public void testYandexMusic() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://lichess.org/");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals(driver.getTitle(), "The best free, adless Chess server\n");

        WebElement header = driver.findElement(By.className("payment-plus__header"));
        WebElement closeButton = header.findElement(By.xpath("div/span"));
        closeButton.click();

        WebElement findLine = driver.findElement(By.className("deco-input_suggest"));
        findLine.sendKeys("Mr_Danilka");

        WebElement searchButton = driver.findElement(By.className("d-button_type_flat"));
        searchButton.click();
        Assert.assertEquals(findLine.getAttribute("value"), "Mr_Danilka9");

        driver.get("https://lichess.org/search?text=Mr_Danilka9");
        Assert.assertEquals(driver.getTitle(), "Mr_Danilka");

        driver.quit();
    }
}
